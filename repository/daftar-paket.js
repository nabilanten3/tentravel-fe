import useSwr from "swr";
import { http } from "../utils/http";

const url = {
  daftarPaket: () => "/products/daftar-paket",
  detaildaftarPaket: (id) => `/products/daftar-paket/${id}`,
};

const hooks = {
  usedaftarPaket() {
    return useSwr(url.daftarPaket(), http.get);
  },
  useDetaildaftarPaket(id) {
    return useSwr(url.detaildaftarPaket(id), http.get);
  },
};

const api = {
  createdaftarPaket(data) {
    return http.post(url.daftarPaket(), data);
  },
  updatedaftarPaket(id, data) {
    return http.put(url.detaildaftarPaket(id), data);
  },
  deletedaftarPaket(id) {
    return http.del(url.detaildaftarPaket(id));
  },
};

export const daftarPaketRepository = { url, hooks, api };

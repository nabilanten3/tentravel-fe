import useSwr from "swr";
import { http } from "../utils/http";

const url = {
  pelanggan: () => "/pelanggan",
  detailpelanggan: (id) => `/pelanggan/${id}`,
};

const hooks = {
  usepelanggan() {
    return useSwr(url.pelanggan(), http.get);
  },
  useDetailpelanggan(id) {
    return useSwr(url.detailpelanggan(id), http.get);
  },
};

const api = {
  createpelanggan(data) {
    return http.post(url.pelanggan(), data);
  },
  updatepelanggan(id, data) {
    return http.put(url.detailpelanggan(id), data);
  },
  deletepelanggan(id) {
    return http.del(url.detailpelanggan(id));
  },
};

export const pelangganRepository = { url, hooks, api };

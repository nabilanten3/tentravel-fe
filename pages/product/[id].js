import { Tabs, ConfigProvider, message, Image, DatePicker } from "antd";
// import Image from "next/image";
import { useRouter } from "next/router";
import React, { useState } from "react";
import Testimonials from "../../components/Testimonials";
import LandingPageLayout from "../../layouts/LandingPageLayout";
import { productsRepository } from "../../repository/products";
import { AiFillPlusSquare, AiFillMinusSquare } from "react-icons/ai";
import { FaShoppingCart } from "react-icons/fa";
import SugestionProducts from "../../components/SugestionProducts";
import { cartRepository } from "../../repository/cart";
import { store } from "../../store/store";
import { daftarPaketRepository } from "../../repository/daftar-paket";
import { userRepository } from "../../repository/user";
import { reservasiRepository } from "../../repository/reservasi";
import { pelangganRepository } from "../../repository/pelanggan";

const DetailProduct = () => {
  // User Context
  const router = useRouter();

  const [date, setDate] = useState("");

  // Router Query for Detail Product
  const { id } = router.query;

  // Fetching Data
  const { data: dataDetailProduct } =
    daftarPaketRepository.hooks.useDetaildaftarPaket(id);
  const detailProduct = dataDetailProduct?.data;

  const { data: dataUser } = pelangganRepository.hooks.usepelanggan();

  const idPelanggans = dataUser?.data?.filter(
    (data) => data?.user?.id === store?.UserStore?.user?.id
  )[0]?.id;

  console.log(dataUser?.data, "datauser");

  // Rupiah Formatter
  const rupiah = (number) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(number);
  };

  const handleBuyItem = async (id) => {
    const data = {
      pelangganId: idPelanggans,
      daftarPaketId: id,
      reservationDate: date,
      image: ["string"],
    };

    try {
      await reservasiRepository.api.createreservasi(data);
      message.success("Silahkan Menuju Halaman transaksi");
    } catch (e) {
      message.error("gagal");
    }

    console.log(data, "datass");
  };

  return (
    <div className="pb-10 pt-28 lg:pt-40">
      <div className="max-w-screen-xl mx-auto">
        <div className="p-6 md:px-10 lg:px-10 xl:p-6">
          <div className="flex flex-col gap-5 lg:flex-row">
            <div className="w-full overflow-hidden rounded-sm">
              <div className="flex flex-col-reverse gap-5 md:flex-row">
                <div className="w-full overflow-hidden">
                  <Image
                    src={`http://localhost:3222/file/${detailProduct?.paket?.image[0]}`}
                    preview={false}
                    className="w-full h-full"
                    alt="Product Image"
                  />
                </div>
              </div>
            </div>
            <div className="flex flex-col w-full mx-2 text-xl">
              <div className="lg:pl-8 space-y-7">
                <h1 className="pt-5 pb-5 text-3xl font-bold lg:pt-0 text-background">
                  {detailProduct?.name}
                </h1>
                <p className="text-sm text-background">
                  Harga :
                  <span className="ml-4 text-lg font-semibold">
                    {rupiah(detailProduct?.price)}
                  </span>
                </p>
                <p className="text-sm text-background">
                  Price :
                  <span className="ml-4 text-lg font-semibold">
                    {detailProduct?.participants} Orang
                  </span>
                </p>
                <p className="text-sm text-background">
                  Fasilitas :
                  <span className="ml-4 text-lg font-semibold">
                    {detailProduct?.paket?.facility}
                  </span>
                </p>
                <p className="text-sm text-background">
                  Rencana Perjalanan :
                  <span className="ml-4 text-lg font-semibold">
                    {detailProduct?.paket?.itinerary}
                  </span>
                </p>
                <div className="text-sm text-background">
                  Pilih Tanggal :{" "}
                  <DatePicker onChange={(date, string) => setDate(string)} />
                </div>
                <div className="flex flex-row gap-5 pt-5">
                  <button
                    className="py-3 text-sm font-medium uppercase duration-300 rounded-sm px-7 bg-background text-softWhite w-full"
                    onClick={() => handleBuyItem(detailProduct?.id)}
                  >
                    Pesan Sekarang
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailProduct;

DetailProduct.getLayout = (page) => (
  <LandingPageLayout title="Encrease - Detail Product" children={page} />
);

import React from "react";
import Hero from "../components/Hero";
import ProductList from "../components/ProductList";
import LandingPageLayout from "../layouts/LandingPageLayout";

const Home = () => {
  return (
    <div>
      <Hero />
      <ProductList />
    </div>
  );
};

export default Home;

Home.getLayout = (page) => (
  <LandingPageLayout title="Encrease - Home" children={page} />
);

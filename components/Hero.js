import React from "react";

const Hero = () => {
  return (
    <div>
      <div className="overflow-hidden">
        <div className="bg-hero flex bg-cover justify-center w-full h-screen bg-right md:bg-center bg-no-repeat">
          <div className="h-screen w-full bg-black/60 text-center flex items-center justify-center font-bold text-5xl lg:text-6xl text-softWhite">
            <h1>Let Us Take You Away!</h1>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hero;

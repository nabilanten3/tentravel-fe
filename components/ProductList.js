import React from "react";
import eifel from "../public/assets/eifel.png";
import bali from "../public/assets/bali.png";
import cappodocia from "../public/assets/cappodocia.png";
import { daftarPaketRepository } from "../repository/daftar-paket";
import { Image } from "antd";
import { useRouter } from "next/router";

const ProductList = () => {
  const router = useRouter();

  const { data: dataProducts } = daftarPaketRepository.hooks.usedaftarPaket();

  // RUPIAH FORMATTER
  const rupiah = (number) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(number);
  };

  return (
    <div className="pt-10 pb-20 bg-softWhite">
      <div className="flex flex-col justify-center gap-y-5 md:gap-y-20">
        <div className="text-center p-4">
          <h1 className="text-3xl font-semibold md:text-4xl text-background">
            Popular Destinations For You
          </h1>
        </div>
        <div className="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-4 max-w-screen-xl mx-auto">
          {dataProducts?.data?.map((product, index) => {
            return (
              <div
                className="bg-gray-50 rounded-md w-72 shadow-lg hover:scale-105 transition duration-300 cursor-pointer"
                key={index}
                onClick={() => router.push(`/product/${product?.id}`)}
              >
                <div className="w-full h-auto rounded-md">
                  <Image
                    src={`http://localhost:3222/file/${product?.paket?.image[0]}`}
                    className="h-1/2 w-full rounded-t-md"
                    preview={false}
                    height={280}
                  />
                </div>
                <div className="p-4">
                  <div>
                    <h1 className="font-semibold text-lg capitalize">
                      {product.name}
                    </h1>
                  </div>
                  <div>
                    <h2 className="text-orange-500 font-bold text-sm">
                      {rupiah(product?.price)}
                    </h2>
                  </div>
                  <div>
                    <h2 className="text-xs">
                      Kapasitas : {product.participants} Orang
                    </h2>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ProductList;
